<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which -no me deja 
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
   
});

// Ruta de controlador BD;

Route::get("categorias" , "CategoriaController@index");  
    
//Ruta que nos muestre el formulario
Route::get("categorias/create","CategoriaController@create");
//Guarda categoria en base de datos
Route::post("categorias/store", "CategoriaController@store");
Route::get('categorias/edit/{category_id}', "CategoriaController@edit");
Route::post('categorias/update/{category_id}', "CategoriaController@update");
