<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.css" integrity="sha256-0XAFLBbK7DgQ8t7mRWU5BF2OMm9tjtfH945Z7TTeNIo=" crossorigin="anonymous" />
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sakila- Lista de Catgeorias</title>
</head>
<body>
    <h1> Lista de Categorías </h1>
    <table class="table table-hoven">
       
            <tr>
                <th>
                    Nombre de Categoría
                </th>
                <th>
                    Actualizar
                </th>
            </tr>
      
             @foreach ($categorias as $c)
                <tr>
                    
                      <td>  {{ $c->name }} </td>
                      <td><a href="{{ url('categorias/edit/'.$c->category_id ) }}" >
                         Actualizar

                         </a>
                      </td>
                </tr>
                
            @endforeach
       
    </table>
    {{ $categorias->links() }}
</body>
</html>