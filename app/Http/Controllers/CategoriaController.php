<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use Illuminate\Support\Facades\Validator;

class CategoriaController extends Controller
{
    //Accion_ UN METOODO DEL CONTROLADOR
    //Nombre: Cualquiera
    //Nombres en minusculas(recomendacion)

    public function index(){
        //seleccionar las categorias existentes 
        $categorias = Categoria::all();

        //enviar la coleccion de categorias a una vista
        // y las vamos a mostrar alli 
        $categorias = Categoria::paginate(5);
        return view("categorias.index")->with("categorias", $categorias);

        
    }
//mostrar el formulario de crear categoria
    public function create (){

        //echo "Formulario de Categoria";
        return view ("categorias.new");

    }
//llegar los datos desde el formulari
//guardar la categoria en BD
    public function store (Request $r){

       //1. Establecer las reglas de validacion para cada campo
       $reglas = [
           "categoria" => ["required", "alpha" ]
       ];

       $mensajes = [
           "required" => "Campo obligatorio",
           "alpha" => "Solo letras"
       ];
       //2. crear objeto  validador

       $validador = Validator::make($r->all(), $reglas, $mensajes );

       //3. Validar: metodo fails
       if($validador->fails()) {
           return redirect("categorias/create")->withErrors($validador);

       }else {

       }



        //%_POST= arreglo de php por defecto 
        // almacena la informacion que procede de formularios
        
        //crear una nueva categoria
        $categoria = new Categoria();
        //Asignar nombre
        //trae datos desde el campo del formulario "categoria"
        $categoria-> name = $r->input("categoria");
        //guardar nueva categoria
        $categoria-> save ();
        // letrero de exito
        return redirect('categorias/create')->with("mensaje","Categoria guardada");

    }

    public function edit ($category_id) {
       // echo $category_id;
       $categoria = Categoria::find($category_id);

       return view("categorias.edit")->with("categoria", $categoria);

    }

    public function update ($category_id) {

      // echo $category_id;
      $categoria = Categoria::find($category_id);
      $categoria->name = $_POST["categoria"];
      $categoria->save ();

      return redirect("categorias/edit/$category_id")->with ("mensaje" , "Categoria editada");


    }



}
